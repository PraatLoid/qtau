#-------------------------------------------------
# http://github.com/qtau-devgroup
#-------------------------------------------------
QT_VERSION = 5
TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += editor \
    plugins/madde_synth \
    plugins/otoedit \
    plugins/vosamp
    
