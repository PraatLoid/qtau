QTau editor (updated version with support for eCantorix2 and UTAULOID support)

Note editor inspired by Vocaloid, UTAU and Cadencii, uses Qt.
 * License: GPLv3
 * External Dependencies: Qt 5, Jack, libsmf, libsamplerate
 * OS: GNU/Linux, Windows (needs to be cross compiled using GNU Guix)
 
Chat rooms (XMPP):
 * qtau(at)rooms.dismail.de
 * qtau_ger(at)rooms.dismail.de

Mailing Lists:
* qtau-dev(at)groups.io
* qtau-user(at)groups.io
* mbrola(at)groups.io
* espeak-ng@(at)roups.io
