/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef INIFILE_H
#define INIFILE_H

class QFile;
class QTextStream;
#include <QFile>
#include <QString>
#include <QTextStream>

class INIFile {
    QFile* file = nullptr;
    QTextStream* stream = nullptr;

protected:
    virtual void handleKey(QString key, QString value) = 0;
    virtual void handleSection(QString section) = 0;
    virtual bool writeOutputToStream() = 0;
    virtual QString codec() {
        return "ASCII";
    }

public:
    INIFile() {}
    bool readFromFile(QString fileName) {
        file = new QFile(fileName);
        if (file->open(QIODevice::ReadOnly | QIODevice::Text)) {
            stream = new QTextStream(file);

            stream->setCodec(this->codec().toStdString().c_str());
            while (!stream->atEnd()) {
                QString line = stream->readLine();

                QStringList kv = line.split("=");

                if (kv.length() == 2) {
                    handleKey(kv[0], kv[1]);
                } else if (line[0] == '[' && line[line.length() - 1] == ']') {
                    handleSection(line.mid(1, line.length() - 2));
                }
            }
        }
        // if(file) delete file;
        // if(stream) delete stream;
        return true;
    }
    bool writeToFile(QString fileName) {
        file = new QFile(fileName);
        if (file->open(QIODevice::WriteOnly | QIODevice::Text)) {
            stream = new QTextStream(file);
            stream->setCodec("UTF8");
            writeOutputToStream();
            // strea,
        }
        if (stream) delete stream;
        if (file) delete file;

        return true;
    }

    void writeSection(QString section) {
        (*stream) << "[" << section << "]\n";
    }

    void writeKey(QString key, QString value) {
        (*stream) << key << "=" << value << "\n";
    }
};

#endif  // INIFILE_H
// http://linuxsagas.digitaleagle.net/2013/09/15/synthesizing-an-accapella-song-with-festival-speech-synthesis/
