/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef PLUGININTERFACES_H
#define PLUGININTERFACES_H

#include <QStringList>
#include <QtPlugin>
class QAction;
#include "Utils.h"

struct selectionRange {
    int start;
    int end;
};

class IController {
public:
    virtual void startPlayback(float pos) = 0;
    virtual void stopPlayback() = 0;
    virtual void logError(const QString& error) = 0;
    virtual void logDebug(const QString& debug) = 0;
    virtual void logSuccess(const QString& success) = 0;
    virtual void addPluginAction(QAction* action) = 0;
    virtual int sampleRate() = 0;
    virtual void startOfflinePlayback(const QString& fileName) = 0;
    virtual QString getCurrentVoiceDirectory()=0;
};

struct synthPoint
{
    float x;
    float y;
};

struct synthNote {
    float rest;
    float lenght;
    float start;
    int pitch;
    QString lyric;
    QList <synthPoint> dyn;
    QList <synthPoint> pit;
    //QStringList pho; // TODO fill
};

class IScore {
public:
    virtual int getNoteCount() = 0;
    virtual synthNote getNote(int index) = 0;
};

class IPreviewSynth {
public:
    virtual ~IPreviewSynth() {}

    virtual QString name() = 0;
    virtual QString description() = 0;
    virtual QString version() = 0;
    virtual void setup(IController* ctrl) = 0;

    virtual void readData(float* data, int length) = 0;
    virtual void start(float fractionalMidiNumber) = 0;
    virtual void stop() = 0;
};

class ISynth {
public:
    virtual ~ISynth() {}

    // startup
    virtual QString name() = 0;
    virtual QString description() = 0;
    virtual QString version() = 0;
    virtual void setup(IController* ctrl) = 0;

    // synth backend
    virtual bool setCacheDir(QString cacheDir) = 0;
    virtual bool setVoicePath(QString voicePath) = 0;
    virtual bool synthesize(IScore* score) = 0;
    virtual bool synthIsRealtime() = 0;
    virtual int readData(float* data, int size) = 0;

    // nlp frontend -- rework needed
    //virtual QString getTranscription(QString) = 0;
    //virtual bool doPhonemeTransformation(QStringList&) = 0;
};

//TODO: IEditorPlugin - for otoedit

#define c_isynth_comname "moe.ecantorix.qtau.ISynth"
#define c_ipreviewsynth_comname "moe.ecantorix.qtau.IPreviewSynth"

Q_DECLARE_INTERFACE(ISynth, c_isynth_comname)
Q_DECLARE_INTERFACE(IPreviewSynth, c_ipreviewsynth_comname)

#endif  // PLUGININTERFACES_H
