/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QPair>
#include <QPoint>
#include <QSize>
#include <QString>
#include <QVariant>
#include <QVector>
#include <QDebug>

#define STR(x) QVariant(x).toString()

#define __FILE_LINE__ (QString(__FILE__)+":"+STR(__LINE__))

#define DEVLOG_ERROR(msg)         qDebug()<<"ERROR"<<__FILE_LINE__<<msg;
#define DEVLOG_WARNING(msg)       qDebug()<<"WARNING"<<__FILE_LINE__<<msg;
#define DEVLOG_DEBUG(msg)         qDebug()<<"DEBUG"<<__FILE_LINE__<<msg;
#define DEVLOG_INFO(msg)          qDebug()<<"INFO"<<__FILE_LINE__<<msg;

const QString c_qtau_name = QStringLiteral("QTau");
const QString c_qtau_ver = QStringLiteral("α1");

/* Pulses Per Quarter note - standard timing resolution of a MIDI sequencer,
     needs to be divisible by 16 and 3 to support 1/64 notes and triplets.
     Used to determine note offset (in bars/seconds) and duration */
const int c_midi_ppq = 480;
const int c_zoom_num = 17;
const int cdef_zoom_index = 4;
const int cdef_note_height = 14;

const int c_zoom_note_widths[c_zoom_num] = {16,  32,  48,  64,  80,  96,
                                            112, 128, 144, 160, 176, 208,
                                            240, 304, 368, 480, 608
                                           };
class TempoMap;

// stores score global values
// TODO replace with proper class
struct SNoteSetup {
    QSize note;  // gui dimensions (in pixels)

    int baseOctave;
    int numOctaves;

    int numBars;
    int* barScreenOffsets;
    TempoMap* tmap;

    inline int getBarForScreenOffset(int x) {
        for (int i = 0; i < numBars; i++) {
            if (barScreenOffsets[i] >= x) {
                return i;
            }
        }
        // this is a error
        return -1;
    }

    int quantize;  // 1/x note length, used as unit of offset for singing notes
    int length;    // 1/x singing note length unit (len=4 means 1/4, 2/4 etc +1/4)

    int octHeight;

    SNoteSetup()
        : note(c_zoom_note_widths[cdef_zoom_index], cdef_note_height),
          baseOctave(1),
          numOctaves(8),
          numBars(128),
          tmap(0),
          quantize(32),
          length(32),
          octHeight(note.height() * 12) {}
};

//----------------------------------------------------

enum class ELog : char { none, info, debug, error, success };

enum class EColor : char { none, green, red };

#define MAXRECENTFILES 8
//#define DEFAULT_SAMPLERATE 44100

#endif  // UTILS_H
