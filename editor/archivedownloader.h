#ifndef ARCHIVEDOWNLOADER_H
#define ARCHIVEDOWNLOADER_H

#include <QObject>
#include <QProcess>
#include <QTemporaryDir>

class ArchiveDownloader : public QObject
{
    Q_OBJECT
    QString url;
    QProcess* p;
    QTemporaryDir* t;
    char phase=0;
    QString target;
public:
    explicit ArchiveDownloader(QString url, QString target, QObject *parent = nullptr);

signals:
    void done();

public slots:
    void processFinished();
};

#endif // ARCHIVEDOWNLOADER_H
