/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef AUDIOENGINE_H
#define AUDIOENGINE_H

#include <QObject>
#include <QThread>
class JackAudio;
class OutputBuffer;
class IPreviewSynth;
// class ISynth;

class qtauController;

class AudioEngine : public QThread {
  JackAudio* _jack = nullptr;
  float* _jack_buffer = nullptr;
  int _jack_buffer_size = 0;
  int _eventfd = 0;
  bool _useJackTransport = false;
  bool _localTransportRolling = false;
  int _pos = 0;
  int _lastJackTransportPos = 0;
  OutputBuffer* _outp = nullptr;
  IPreviewSynth* _preview = nullptr;
  bool _running = true;
  Q_OBJECT
 public:
  explicit AudioEngine(JackAudio* jack, QObject* parent = 0);
  void run() override;
  bool useJackTransport() { return _useJackTransport; }
  void setLocalTransportRolling(bool value) { _localTransportRolling = value; }
  void setUseJackTransport(bool value) { _useJackTransport = value; }
  void setOutputBuffer(OutputBuffer* outp) { _outp = outp; }
  void setPreviewSynth(IPreviewSynth* psynth) { _preview = psynth; }
  int transportPosition() { return _pos; }
  void setTransportPosition(int value) { _pos = value; }
  bool localTransportRolling() { return _localTransportRolling; }
  void shutdown();
 signals:

 public slots:
};

#endif  // AUDIOENGINE_H
