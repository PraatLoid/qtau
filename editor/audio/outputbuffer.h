/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef OUTPUTBUFFER_H
#define OUTPUTBUFFER_H

#include <jack/ringbuffer.h>
#include <sndfile.h>
#include <QObject>
#include <QString>
#include <QThread>
#include "../PluginInterfaces.h"
#include "audio/jackaudio.h"

class ISynth;

class OutputBuffer : public QThread {
 private:
  void run() override;
  jack_ringbuffer_t* _ringbuffer = nullptr;
  int _datacount = 0;
  int _jackSamplerate = 0;
  SNDFILE* _sndfile = nullptr;
  bool _playbackIsStable = false;
  int _buffersize = 0;
  ISynth* _scheduledSynth = nullptr;
  int _xruncount = 0;
  int _runtime = 0;
  bool _offline = false;
  Q_OBJECT
 signals:
  void startPlayback();
  void stopPlayback();
 public slots:

 public:
  OutputBuffer(JackAudio* jack);
  virtual void open(QString fileName, int samplerate);
  virtual void openReadFile(QString fileName);
  virtual void close();
  virtual void readData(float* data, int size);
  virtual void reset();
  void scheduleSynth(ISynth* synth);
};

#endif  // OUTPUTBUFFER_H
