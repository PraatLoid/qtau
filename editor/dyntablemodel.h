#ifndef DYNTABLEMODEL_H
#define DYNTABLEMODEL_H

#include <QAbstractTableModel>
#include <NoteEvents.h>

class DynTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    DynTableModel();
    void loadOne(quint64 sel1,QJsonObject o);
    void reset();
    bool hasChanges(QVector<quint64> sel);
    void saveChanges();

protected:
    Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    Q_INVOKABLE virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags(const QModelIndex &index) const override;

private:
    struct DynTableData
    {
        QString key;
        QString value;
        QString valueOrig;
        bool updated=false;
    };
    QVector<DynTableData*> _data;
    quint64 _sel1=-1;
signals:
    void noteEffect(qtauEvent* e);
};

#endif // DYNTABLEMODEL_H
