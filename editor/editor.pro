#-------------------------------------------------
# http://github.com/qtau-devgroup/editor
#-------------------------------------------------

QT += core widgets network

TARGET = QTau
TEMPLATE = app

INCLUDEPATH += ../tools
PKGCONFIG += glib-2.0

LIBS += -lsmf -lsinsy -lsndfile

CONFIG      += link_pkgconfig

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Session.cpp \
    Controller.cpp \
    ui/piano.cpp \
    ui/noteEditor.cpp \
    ui/dynDrawer.cpp \
    ui/meter.cpp \
    Utils.cpp \
    ui/noteEditorHandlers.cpp \
    ui/tempodialog.cpp \
    audio/jackaudio.cpp \
    audio/audioengine.cpp \
    audio/outputbuffer.cpp \
    sinsyscoreconverter.cpp \
    midifile.cpp \
    tempomap.cpp \
    ustfile.cpp \
    score.cpp \
    dyntablemodel.cpp \
    archivedownloader.cpp



HEADERS  += \
    mainwindow.h \
    PluginInterfaces.h \
    Events.h \
    NoteEvents.h \
    Controller.h \
    Session.h \
    ui/piano.h \
    ui/noteEditor.h \
    ui/dynDrawer.h \
    ui/meter.h \
    ui/Config.h \
    Utils.h \
    ui/noteEditorHandlers.h \
    ui/tempodialog.h \
    audio/jackaudio.h \
    audio/audioengine.h \
    audio/outputbuffer.h \
    sinsyscoreconverter.h \
    midifile.h \
    tempomap.h \
    ustfile.h \
    score.h \
    dyntablemodel.h \
    archivedownloader.h

TRANSLATIONS = res/qtau_de.ts

FORMS += ui/mainwindow.ui \
    ui/tempodialog.ui

RESOURCES += res/qtau.qrc

windows:RC_FILE = res/qtau_win.rc

QMAKE_CXXFLAGS += -Wall -std=c++11

#--------------------------------------------
CONFIG(debug, debug|release) {
    DESTDIR = $${OUT_PWD}/../debug
    LIBS += -Wl,--no-undefined
} else {
    DESTDIR = $${OUT_PWD}/../release
}

OBJECTS_DIR     = $${DESTDIR}/editor/.obj
MOC_DIR         = $${DESTDIR}/editor/.moc
RCC_DIR         = $${DESTDIR}/editor/.rcc
UI_DIR          = $${DESTDIR}/editor/.ui
#--------------------------------------------

INCLUDEPATH += ../tools/libogg-1.3.1/include ../tools/flac-1.3.0/include ../tools/flac-1.3.0/src/libFLAC/include

DEFINES += HAVE_CONFIG_H

QMAKE_CFLAGS += -std=c99

LIBS += -ljack




unix {
    target.path = $${PREFIX}/bin
    desktop.path    = $${PREFIX}/share/applications
    desktop.files   += res/qtau.desktop
    appicon.path    = $${PREFIX}/share/qtau/icons
    appicon.files   += res/appicon_ouka_alice.png
    INSTALLS += target desktop appicon
}


