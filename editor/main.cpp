/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include <QApplication>
#include <QIcon>
#include "Controller.h"
#include "Utils.h"

#include <errno.h>
#include <sys/file.h>
#include <unistd.h>

#include <QTranslator>

//GUI erweitern : Oktober 2020 +
// Connichi: feature freeze

#define __devloglevel__ 8

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    QTranslator trans;

    if (trans.load(QLocale(), QLatin1String("qtau"), QLatin1String("_"),
                   QLatin1String(":/tr")))
        QCoreApplication::installTranslator(&trans);
    else
        DEVLOG_ERROR("failed to setup translator");

    DEVLOG_DEBUG("trans: " + QLocale::system().name());
    app.installTranslator(&trans);

    app.setWindowIcon(QIcon(":/images/appicon_ouka_alice.png"));

    int pid_file = open("/tmp/qtau.lock", O_CREAT | O_RDWR, 0666);
    int rc = flock(pid_file, LOCK_EX | LOCK_NB);
    if (rc) {
        if (EWOULDBLOCK == errno) {
            DEVLOG_ERROR("qtau is running");
            return rc;
        }
    }

    qtauController c(app.applicationDirPath());
    c.run();  // create UI that can immediately call back, thus requires already
    // created & inited controller

    rc = app.exec();
    unlink("/tmp/qtau.lock");
    c.shutdown(rc);  // kill all threads
    return rc;
}
