/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include "PluginInterfaces.h"

#include "Utils.h"
#include "tempomap.h"

class qtauController;
class qtauSession;
class qtauEvent;
class qtauPiano;
class qtauNoteEditor;
class qtauMeterBar;
class qtauDynDrawer;
class qtauDynLabel;
class qtauWaveform;
class MeterBarLineEdit;
class DynTableModel;

class QAction;
class QScrollBar;
class QSlider;
class QToolBar;
class QTabWidget;
class QTextEdit;
class QToolBar;
class QSplitter;
class QComboBox;
class QLabel;
class QLineEdit;
class QTableView;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool setController(qtauController &c, qtauSession &s);

private:
    Ui::MainWindow *ui;

private:
    void updateUndoRedoTexts();
    void updateTMap();

signals:
    void loadUST(QString fileName);
    void saveUST(QString fileName, bool rewrite);
    void saveAudio(QString fileName, bool rewrite);

    void loadAudio(QString fileName);
    void setVolume(int);

public slots:
    void onLog(const QString &, ELog);
    void updateRecentFileActions();
    void onOpenUST();
    void onSaveUST();
    void onSaveUSTAs();

    void notesVScrolled(int);
    void notesHScrolled(int);
    void vertScrolled(int);
    void horzScrolled(int);

    void onEditorRMBScrolled(QPoint, QPoint);
    void onEditorRequestOffset(QPoint);

    void onPianoHeightChanged(int newHeight);
    void onNoteEditorWidthChanged(int newWidth);

    void onUndo();
    void onRedo();
    void onDelete();

    void onEditMode(bool);
    void onGridSnap(bool);
    void onQuantizeSelected(int);
    void onNotelengthSelected(int);

    void dynBtnLClicked();
    void dynBtnRClicked();

    void onTabSelected(int);
    void onZoomed(int);
    void onEditorZoomed(int);

    void onSingerSelected(int);

    void onDocReloaded();
    void onDocStatus(bool);
    void onUndoStatus(bool);
    void onRedoStatus(bool);
    void onDocEvent(qtauEvent *);

    void onTransportPositionChanged(float pos);

    void onMIDIImport();
    void onMIDIExport();
    void onActionJackTriggered();

    void onSelectionChanged();

    void markOverlappingNotes(quint64 id1, quint64 id2);
    void updateNoteColors();

    void meter_barClicked(int bar, tmlabel mode);
    void mbe_keyPressHooked(int key);
    void voiceListChanged();

protected:
    qtauSession *_doc=nullptr;
    qtauController *_ctrl=nullptr;
    SNoteSetup _ns;
    QTabWidget *_tabs=nullptr;
    QComboBox *_voiceNameCbo=nullptr;

    qtauPiano *_piano;
    qtauNoteEditor *_noteEditor=nullptr;
    qtauDynDrawer *_drawZone=nullptr;
    qtauMeterBar *_meter=nullptr;

    MeterBarLineEdit *_mbe=nullptr;
    QLabel *_mbl=nullptr;

    QAction *_recentFileActs[MAXRECENTFILES];

    QWidget *_wavePanel = nullptr;  // used to switch its visibility, hidden by default
    QWidget *_drawZonePanel = nullptr;
    QSplitter *_editorSplitter = nullptr;

    //remove
    qtauDynLabel *_fgDynLbl = nullptr;
    qtauDynLabel *_bgDynLbl = nullptr;

    QScrollBar *_hscr=nullptr;
    QScrollBar *_vscr=nullptr;
    QSlider *_zoom=nullptr;
    QComboBox *_singerSelect=nullptr;

    QTextEdit *_logpad=nullptr;
    QList<QToolBar *> _toolbars;
    QTableView* _dynTable;
    DynTableModel* _dynTableModel;

    void enableToolbars(bool enable = true);

    QColor _logTabTextColor;
    int _logNewMessages= 0;
    bool _logHasErrors = false;
    bool _showNewLogNumber = false;

    QString _docName;
    QString _lastScoreDir;
    QString _lastAudioDir;
    QString _audioExt;

    int _current_bar;
    tmlabel _current_mode = TM_ALL;

    QMenu *_menuPlugins = nullptr;

    void updateSetup();

    void dragEnterEvent(QDragEnterEvent *);
    void dragMoveEvent(QDragMoveEvent *);
    void dropEvent(QDropEvent *);
    void closeEvent(QCloseEvent *);

private slots:
    void on_actionPhoneme_Transformation_triggered();
    void openRecentFile();

    void on_actionSave_Last_Play_triggered();

    void on_actionMusicXML_triggered();

    void on_actionUST_triggered();

    void on_actionEdit_Tempo_Time_Signature_triggered();

    void on_actionConnect2Jack_triggered();

public:
    void addPluginAction(QAction *);
    struct selectionRange getSelectionRange();
    TempoMap *getTempoMap() {
        return _ns.tmap;
    }
};

#endif  // MAINWINDOW_H
