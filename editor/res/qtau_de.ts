<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui/mainwindow.ui" line="20"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="34"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="38"/>
        <source>Import</source>
        <translation type="unfinished">Importieren</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="46"/>
        <source>Export as...</source>
        <translation type="unfinished">Exportieren als...</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="52"/>
        <source>Recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="69"/>
        <source>Transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="79"/>
        <source>Edit</source>
        <translation type="unfinished">Bearbeiten</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="92"/>
        <source>Lyrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="100"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="115"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="118"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="127"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="130"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="142"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="145"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="154"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="157"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="169"/>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="172"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="184"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="187"/>
        <source>Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="196"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="199"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="211"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="214"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="226"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="229"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="238"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="241"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="253"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="256"/>
        <source>Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="268"/>
        <source>Repeat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="271"/>
        <source>Shift+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="286"/>
        <source>Grid Snap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="289"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="301"/>
        <source>Edit Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="304"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="309"/>
        <location filename="ui/mainwindow.ui" line="314"/>
        <source>MIDI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="326"/>
        <source>Save audio as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="331"/>
        <source>Phoneme Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="336"/>
        <source>User Word Dictionary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="341"/>
        <source>Note Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="356"/>
        <source>Use Jack Transport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="361"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="366"/>
        <location filename="mainwindow.cpp" line="1129"/>
        <source>Save Last Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="371"/>
        <source>MusicXML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="376"/>
        <source>UTAU Sequence Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="381"/>
        <source>TempoMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="386"/>
        <source>Edit Tempo / Time Signature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <location filename="mainwindow.cpp" line="852"/>
        <location filename="mainwindow.cpp" line="883"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="594"/>
        <source>Open USTJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="594"/>
        <location filename="mainwindow.cpp" line="614"/>
        <source>UTAU JSON Sequence Text Files (*.ustj)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Save USTJ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="630"/>
        <source>Import MIDI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="632"/>
        <location filename="mainwindow.cpp" line="640"/>
        <source>MIDI Files (*.mid *.mid *.smf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Export as MIDI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1087"/>
        <source>&amp;%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1129"/>
        <source>WAV Files (*.wav)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1149"/>
        <location filename="mainwindow.cpp" line="1157"/>
        <source>Import MusicXML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1151"/>
        <source>MusicXML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1159"/>
        <source>UTAU Sequence Text Files (*.ust)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TempoDialog</name>
    <message>
        <location filename="ui/tempodialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/tempodialog.ui" line="65"/>
        <source>Add tempo change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/tempodialog.ui" line="78"/>
        <source>Add timesignature change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/tempodialog.ui" line="91"/>
        <source>Remove selected event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/tempodialog.ui" line="104"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui/tempodialog.ui" line="127"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
