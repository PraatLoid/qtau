#ifndef QTAUSCORE_H
#define QTAUSCORE_H

#include <QJsonArray>
#include "PluginInterfaces.h"
#include "tempomap.h"

class QtauScore : public IScore {
    std::vector<synthNote> notes;

    float last_DYN=0;
    float last_PIT=0;

    void fillTrack(QString type,QStringList events,synthNote* note);
    float checkInc(QString type,float lastpos);

public:
    QtauScore(const QJsonArray& ust, TempoMap* tmap);
    int getNoteCount();
    synthNote getNote(int index);
};

#endif  // QTAUSCORE_H
