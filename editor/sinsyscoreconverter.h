/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef SINSYSCORECONVERTER_H
#define SINSYSCORECONVERTER_H

#undef emit
#include <sinsy/sinsy.h>
#define emit Q_EMIT
#include <QJsonArray>
#include "tempomap.h"

class SinsyScoreConverter : public sinsy::IScore {
    int pos_pulses;
    QJsonArray notes;
    TempoMap tmap;
    int current_measure=0;
    int numerator=4;
    int denominator=4;
    //void genSetup();

public:
    SinsyScoreConverter();
    virtual ~SinsyScoreConverter() {}

    //! set encoding
    virtual bool setEncoding(const std::string& encoding);

    //! add key mark
    virtual bool addKeyMark(sinsy::ModeType modeType, int fifths);

    //! add beat mark (beats/beatType) to end of score: default beat mark is 4/4
    virtual bool addBeatMark(size_t beats, size_t beatType);

    //! add tempo mark to end of score: default tempo is 100bps
    virtual bool addTempoMark(double tempo);

    //! add dynamics mark (sudden changes) to end of score
    virtual bool addSuddenDynamicsMark(
        sinsy::SuddenDynamicsType suddenDynamicsType);

    //! add dynamics mark (gradual changes) to end of score
    virtual bool addGradualDynamicsMark(
        sinsy::GradualDynamicsType gradualDynamicsType);

    //! add note to end of score
    virtual bool addNote(size_t duration, const std::string& lyric, size_t pitch,
                         bool accent, bool staccato, sinsy::TieType tieType,
                         sinsy::SlurType slurType,
                         sinsy::SyllabicType syllabicType, bool breath = false);

    //! add rest to end of score
    virtual bool addRest(size_t duration);

    void toJSON(QJsonArray& ust);


};

#endif  // SINSYSCORECONVERTER_H
