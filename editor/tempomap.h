/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include <QAbstractItemModel>
#include <QJsonArray>
#include <QMap>

#ifndef TEMPOMAP_H
#define TEMPOMAP_H

struct fraction {
    int numerator;
    int denominator;
};

struct tempoindex {
    int pos;
    int type;
};

enum tmlabel {
    TM_BPM = 0,
    TM_SIG = 1,
    TM_ALL = 2,
};

class TempoMap : public QAbstractItemModel {
    QMap<int, float> _tempo;
    QMap<int, fraction> _time;
    QList<tempoindex> _index;
    QMap<int, float> _origTempo;
    QMap<int, fraction> _origTime;
    QList<tempoindex> _origIndex;
    void updateIndexList();

public:
    TempoMap();
    void addTempo(int pos, float tempo);
    void addTimeSignature(int pos, int numerator, int denominator);
    void removeEventAt(int index);
    void removeTempoForBar(int pos);
    void removeTimeSignatureForBar(int pos);
    fraction getTimeSignatureForBar(int pos);
    //
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    //
    void beginEditing();
    void toJson(QJsonArray &array);
    void fromJson(QJsonArray &array);
    void undo();
    bool getBar(int pulses, float &time, int &bar, int end);
    float getBPMforBar(int bar);
    QString getLabel(int bar, tmlabel mode);
    bool setLabel(tmlabel mode, int bar, QString label);
};

#endif  // TEMPOMAP_H
