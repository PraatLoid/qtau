/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef DYNDRAWER_H
#define DYNDRAWER_H

#include "Utils.h"

#include <QLabel>

class QPixmap;
class qtauNoteEditor;
class DynamicsModel;
class QTextEdit;

class qtauDynLabel : public QLabel {
  Q_OBJECT

 public:
  explicit qtauDynLabel(const QString &txt = "", QWidget *parent = 0);
  ~qtauDynLabel();

  typedef enum { off = 0, front, back } EState;

  EState state();
  void setState(EState s);

 signals:
  void leftClicked();
  void rightClicked();

 protected:
  void mousePressEvent(QMouseEvent *event);

  EState _state;
};

//----------------------------------------------

class qtauDynDrawer : public QWidget {
  Q_OBJECT

 public:
  qtauDynDrawer(qtauNoteEditor *qne, QWidget *parent = 0);
  ~qtauDynDrawer();

  void setOffset(int off);
  void configure(const SNoteSetup &newSetup);

 signals:
  void scrolled(int delta);
  void zoomed(int delta);

 public slots:
  void onRepaintDynDrawer(void);

 protected:
  void paintEvent(QPaintEvent *event);
  void resizeEvent(QResizeEvent *event);

  void mouseDoubleClickEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *event);
  //void keyPressEvent(QKeyEvent *event);

  int _offset;
  qtauNoteEditor *_editor;
  SNoteSetup _ns;
  bool _velSelected = true;

  QPoint _absFirstClickPos;
  bool _mousePressed = false;

  int _hSt;
  int _h;
  int _notePos;

  uint64_t _noteUnderCursor;
  float _velocity;

  DynamicsModel* _dyn=nullptr;
  QTextEdit* _edit=nullptr;

  QPixmap *_bgCache;
  void updateCache();
};

#endif  // DYNDRAWER_H
