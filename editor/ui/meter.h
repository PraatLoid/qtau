/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef METER_H
#define METER_H

#include <QLineEdit>
#include <QWidget>
#include "Utils.h"
#include "tempomap.h"

class QPixmap;

class qtauMeterBar : public QWidget {
  Q_OBJECT

 public:
  qtauMeterBar(QWidget *parent = 0);
  ~qtauMeterBar();

  void setOffset(int off);
  void configure2(SNoteSetup &newSetup);

 signals:
  void scrolled(int delta);
  void zoomed(int delta);
  void barClicked(int bar, tmlabel mode);

 public slots:

 protected:
  void paintEvent(QPaintEvent *event);
  void resizeEvent(QResizeEvent *event);

  void mouseDoubleClickEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *event);

  int _offset;
  SNoteSetup _ns;
};

class MeterBarLineEdit : public QLineEdit {
  Q_OBJECT
 public:
  MeterBarLineEdit();

 protected:
  void keyPressEvent(QKeyEvent *event);
 signals:
  void keyPressHooked(int key);
};

#endif  // METER_H
