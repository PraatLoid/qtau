/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include "tempodialog.h"
#include "tempomap.h"
#include "ui_tempodialog.h"

TempoDialog::TempoDialog(TempoMap* tmap, QWidget* parent)
    : QDialog(parent), ui(new Ui::TempoDialog) {
  ui->setupUi(this);

  _tmap = tmap;

  ui->tableView->setModel(tmap);
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

TempoDialog::~TempoDialog() { delete ui; }

void TempoDialog::on_btnTempo_clicked() {
  int pos = QVariant(this->ui->txtPos->text()).toInt();
  int val = QVariant(this->ui->txtValue->text()).toInt();
  if (pos > 0 && val > 0) _tmap->addTempo(pos - 1, val);
}

void TempoDialog::on_btnTimeSignature_clicked() {
  int pos = QVariant(this->ui->txtPos->text()).toInt();
  QStringList l = this->ui->txtValue->text().split("/");
  if (pos > 0 && l.length() == 2)
    _tmap->addTimeSignature(pos - 1, QVariant(l[0]).toInt(),
                            QVariant(l[1]).toInt());
}

void TempoDialog::on_pushButton_3_clicked() {
  QModelIndexList list =
      this->ui->tableView->selectionModel()->selectedIndexes();
  if (list.count() > 0) {
    _tmap->removeEventAt(list[0].row());
  }
}

// TODO: connect gui components, serialize as json
