#ifndef USTFILE_H
#define USTFILE_H

#include <QJsonArray>
#include <QJsonObject>
#include "INIFile.h"

class USTFile : public INIFile {
public:
    USTFile();
    QJsonArray json() {
        return _ust;
    }

protected:
    void handleKey(QString key, QString value);
    void handleSection(QString section);
    bool writeOutputToStream();
    virtual QString codec() {
        return "ISO 8859-1";
    }

private:
    bool _isSetting = false;
    bool _isNote = false;
    int _noteCounter = -1;
    int _posPulses = 0;
    float _tempo = 120;
    QJsonArray _ust;
    QJsonObject _note;
    void genSetup();
};

#endif  // USTFILE_H
