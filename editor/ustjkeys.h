/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef UST_NOTE_KEYS
#define UST_NOTE_KEYS
#define NOTE_LYRIC "lyric"
#define NOTE_PULSE_OFFSET "pulseOffset"
#define NOTE_PULSE_LENGTH "pulseLength"
#define NOTE_KEY_NUMBER "keyNumber"
#define NOTE_VELOCITY "velocity"
#define NOTE_INTENSITY "intensity"
#define NOTE_MODULATION "modulation"

#define NOTE_DYNAMICS "DYN"
#define NOTE_PITCH "PIT"
#define NOTE_PHO "PHO"


#define USER_AGENT "userAgent"
#define SINGER_NAME "singerName"
#define TEMPOMAP "tempomap"

// sinsy
#define XML_ACCENT "accent"
#define XML_STACATTO "stacatto"
#define XML_TIETYPE "tieType"
#define XML_SLURTYPE "slurType"
#define XML_SYLLABICTYPE "syllabicType"
#define XML_BREATH "breath"

#endif
