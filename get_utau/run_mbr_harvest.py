import glob
import subprocess

for wav in glob.glob("*.wav"):
        f0 = wav.replace("wav","f0")
        print("harvesting "+ wav)
        subprocess.call(["mbr-harvest",wav,"-o",f0])
