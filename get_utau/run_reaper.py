import glob
import subprocess

for wav in glob.glob("*.wav"):
        f0 = wav.replace("wav","f0")
        pmk = wav.replace("wav","pmk")
        print(wav)
        subprocess.call(["reaper","-i",wav,"-a","-f",f0,"-p",pmk,"-e","0.005"])
