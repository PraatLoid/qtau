import glob
import subprocess
for wav in glob.glob("*.wav"):
    harvest = wav.replace(".wav",".harvest")
    ogg = wav.replace(".wav",".ogg")
    print(harvest)
    subprocess.call(["mbr-harvest",wav,"-o",harvest])
    print(ogg)
    subprocess.call(["mbr-resynth",wav,harvest,"-o",ogg])
