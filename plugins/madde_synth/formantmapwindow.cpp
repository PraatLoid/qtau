/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include "formantmapwindow.h"
#include <QMouseEvent>
#include "ui_formantmapwindow.h"

FormantMapWindow::FormantMapWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::FormantMapWindow) {
  ui->setupUi(this);
  QPixmap bkgnd(":/images/kth_formant.png");
  QPalette palette;
  palette.setBrush(QPalette::Background, bkgnd);
  this->setPalette(palette);
  this->setFixedHeight(266);
  this->setFixedWidth(266);
  this->setWindowTitle("F1/F2");
}

FormantMapWindow::~FormantMapWindow() { delete ui; }

void FormantMapWindow::mousePressEvent(QMouseEvent *event) {
  float f1 = 200 + (event->y() - 14) * 3.5;
  float f2 = 500 - (event->x() - 252) * 7.5;

  emit mouseDown(f1, f2);
}
void FormantMapWindow::mouseReleaseEvent(QMouseEvent *event) {
  (void)event;
  emit mouseUp();
}

void FormantMapWindow::closeEvent(QCloseEvent *event) {
  emit closed();
  QMainWindow::closeEvent(event);
}
