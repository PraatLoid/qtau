/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef FORMANTMAPWINDOW_H
#define FORMANTMAPWINDOW_H

#include <QMainWindow>

namespace Ui {
class FormantMapWindow;
}

class FormantMapWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit FormantMapWindow(QWidget *parent = 0);
  ~FormantMapWindow();

 protected:
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void closeEvent(QCloseEvent *event);

 private:
  Ui::FormantMapWindow *ui;

 signals:
  void mouseDown(float f1, float f2);
  void mouseUp();
  void closed();
};

#endif  // FORMANTMAPWINDOW_H
