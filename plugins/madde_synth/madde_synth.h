/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef MADDESYNTH_H
#define MADDESYNTH_H

#include <sekai/epr.h>
#include <sekai/hzosc.h>
#include <QThread>
#include <QVariant>
#include "PluginInterfaces.h"
#include "jack/ringbuffer.h"
#include "sekai/common.h"

#define RES_COUNT 6

class WorkerThread;
class MaddeWindow;

class MaddeLOIDSynth : public QObject, public IPreviewSynth {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID c_ipreviewsynth_comname FILE "madde_synth.json")
  Q_INTERFACES(IPreviewSynth)

 public:
  MaddeLOIDSynth() {}
  QString name() override;
  QString description() override;
  QString version() override;
  virtual void setup(IController* ctrl) override;
  virtual void readData(float* data, int length) override;
  virtual void start(float frequency) override;
  virtual void stop() override;

 private:
  int run();

  float _f0 = 0;
  float _phase = 0;

  IFFTOsc _osc;
  EprResonance _res[RES_COUNT];
  bool _resNeedsUpdate[RES_COUNT];
  bool _resEnabled[RES_COUNT];
  EprSourceParams _src;

  jack_ringbuffer_t* _ringbuffer = nullptr;
  bool _threadRunning = false;
  IController* _ctrl = nullptr;
  MaddeWindow* _madde = nullptr;
 public slots:
  void setup2();
  void madde_triggered();
  void madde_valueChanged(QString key, QVariant value);
  void madde_closed();
  void loadDefault();
  void loadFile(QString fileName);
  void saveToFile(QString fileName);
  void mouseEvent(int status);
};

#endif
