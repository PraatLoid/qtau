#-------------------------------------------------
#-------------------------------------------------

TEMPLATE = lib
CONFIG  += plugin
TARGET   = $$qtLibraryTarget(maddesynth)

QT += widgets

INCLUDEPATH += ../../editor ../ ../../tools




# rename sekai to eprsynth
LIBS += -lsekai -ljack -lsndfile

CONFIG      += link_pkgconfig


HEADERS += \
    madde_synth.h \
    ../../editor/Utils.h \
    maddewindow.h \
    maddefile.h \
    formantmapwindow.h

SOURCES += \
    madde_synth.cpp \
    ../../editor/Utils.cpp \
    maddewindow.cpp \
    maddefile.cpp \
    formantmapwindow.cpp

QMAKE_CXXFLAGS += -Wall -std=c++11

#--------------------------------------------
CONFIG(debug, debug|release) {
    COMPILEDIR = $${OUT_PWD}/../../debug
    LIBS += -Wl,--no-undefined
} else {
    COMPILEDIR = $${OUT_PWD}/../../release
    LIBS += -Wl,--no-undefined
}

DESTDIR         = $${COMPILEDIR}/plugins
OBJECTS_DIR     = $${COMPILEDIR}/maddesynth/.obj
MOC_DIR         = $${COMPILEDIR}/maddesynth/.moc
RCC_DIR         = $${COMPILEDIR}/maddesynth/.rcc
UI_DIR          = $${COMPILEDIR}/maddesynth/.ui
#--------------------------------------------

FORMS += \
    maddewindow.ui \
    formantmapwindow.ui

RESOURCES += \
    madde.qrc

unix {
    target.path = $${PREFIX}/lib/qtau/plugins
    INSTALLS += target
}
