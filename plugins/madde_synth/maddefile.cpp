/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include "maddefile.h"
#include "Utils.h"
#define __devloglevel__ 10

// int testfunc();
// static const int x = testfunc();

MaddeFile::MaddeFile() {}
void MaddeFile::handleKey(QString key, QString value) {
  if (currentSection == "Formants") formants[key] = value;
  if (currentSection == "EpRSource") eprsource[key] = value;
}
void MaddeFile::handleSection(QString section) { currentSection = section; }
QVariant MaddeFile::getFormant(QString key) { return formants[key]; }
void MaddeFile::setFormant(QString key, QVariant formant) {
  DEVLOG_DEBUG(key + "=" + formant.toString());
  formants[key] = formant;
}

bool MaddeFile::writeOutputToStream() {
  writeSection("Formants");
  foreach (QString key, formants.keys()) {
    writeKey(key, formants[key].toString());
  }
  writeSection("EpRSource");
  foreach (QString key, eprsource.keys()) {
    writeKey(key, eprsource[key].toString());
  }
  return true;
}
void MaddeFile::setEpRSource(QString key, QVariant value) {
  eprsource[key] = value;
}
QVariant MaddeFile::getEpRSource(QString key) { return eprsource[key]; }
