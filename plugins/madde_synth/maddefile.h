/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef MADDEFILE_H
#define MADDEFILE_H

#include <QMap>
#include <QVariant>
#include "INIFile.h"

class MaddeFile : public INIFile {
  QString currentSection;
  QMap<QString, QVariant> formants;
  QMap<QString, QVariant> eprsource;

 protected:
  virtual void handleKey(QString key, QString value);
  virtual void handleSection(QString section);
  virtual bool writeOutputToStream();

 public:
  MaddeFile();
  QVariant getFormant(QString key);
  void setFormant(QString key, QVariant formant);
  QVariant getEpRSource(QString key);
  void setEpRSource(QString key, QVariant value);
};

#endif  // MADDEFILE_H
