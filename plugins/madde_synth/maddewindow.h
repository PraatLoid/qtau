/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef MADDEWINDOW_H
#define MADDEWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QString>
#include <QVariant>
#include "Utils.h"
#include "formantmapwindow.h"

namespace Ui {
class MaddeWindow;
}

class ValueBinder;

class MaddeWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MaddeWindow(QWidget* parent = 0);
  void setValue(QString key, QVariant value);
  ~MaddeWindow();

 private:
  Ui::MaddeWindow* ui;
  QWidget* bindValue(QWidget* widget, QString keypath);
  QMap<QString, ValueBinder*> valueMap;
  QAction* _recentFileActs[MAXRECENTFILES];
  FormantMapWindow* _formantWindow;
  QString _currentFileName;
  QString _lastMaddeDir;

 private:
  void updateRecentFileActions();
  void updateWindowTitle();
  void addFileToRecentFiles(QString fileName);
 private slots:
  void input_valueChanged(QString key, QVariant value);
  void on_actionNew_triggered();
  void openRecentFile();
  void on_actionOpen_triggered();

  void on_actionSave_triggered();

  void on_actionSave_as_triggered();

  void on_actionShow_F1_F2_map_triggered();
  void formantWindow_mouseDown(float f1, float f2);
  void formantWindow_mouseUp();
  void formantWindow_closed();

 signals:
  void valueChanged(QString key, QVariant value);
  void loadDefault();
  void loadFile(QString fileName);
  void saveToFile(QString fileName);
  void closed();
  void mouseEvent(int status);

 protected:
  void closeEvent(QCloseEvent* event);
};

class ValueBinder : public QObject {
  Q_OBJECT
  friend class MaddeWindow;

 private:
  explicit ValueBinder(QWidget* widget, QString name, MaddeWindow* parent);
  QWidget* _widget;
  QString _name;
  QVariant _value;  // !!! do not set directly !!!
  void setValue(QVariant value);
 private slots:
  void lineEdit_editingFinished();
  void lineEdit_keyPressHooked(int key);
  void checkBox_toggled(bool);
 signals:
  void valueChanged(QString key, QVariant value);
};

// Move to Libkawaii (new one, library with QT extensions, external project)
#include <QLineEdit>
class KawaiiLineEdit : public QLineEdit {
  Q_OBJECT
 public:
  KawaiiLineEdit();

 protected:
  void keyPressEvent(QKeyEvent* event);
 signals:
  void keyPressHooked(int key);
};

#endif  // MADDEWINDOW_H
