/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

// a clone of nwp8861's setParam

#include <sekai/midi.h>

/* --------------------------------------------------------------------------- *
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
* --------------------------------------------------------------------------- */

#define __devloglevel__ 20

#include <unistd.h>
#include <QAction>
#include <QTimer>
#include <QtConcurrent/QtConcurrent>
#include "otoedit.h"
#include "otoini.h"
#include "otoeditwindow.h"



QString OtoEdit::name() { return "OtoEdit"; }
QString OtoEdit::description() {
  return "An editor for UTAU voicebanks";
}
QString OtoEdit::version() { return "18.04"; }

void OtoEdit::start(float keyNum) {}
void OtoEdit::stop() { _f0 = 0; }

void OtoEdit::readData(float* data, int dataLength) {
}


void OtoEdit::setup_ui()  // rename to setup_delayed
{
  QAction* otoedit = new QAction("OtoEdit", this);
  _ctrl->addPluginAction(otoedit);
  connect(otoedit, SIGNAL(triggered()), this, SLOT(editor_triggered()));
}

void OtoEdit::editor_triggered() {
    if (_oto) return;

    _oto = new OtoEditWindow();
    _oto->setVisible(true);

    QString voicedir = _ctrl->getCurrentVoiceDirectory();
    _oto->setWindowTitle(voicedir + " - OtoEdit");

    connect(_oto, SIGNAL(closed()), this, SLOT(editor_closed()));

    // list subdirs with an oto.ini
    QDirIterator it(voicedir, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QString obj = it.next();
        if (obj.endsWith("oto.ini"))
        _oto->addTab(obj);
    }
}

void OtoEdit::setup(IController* ctrl) {
  _ctrl = ctrl;

  QTimer::singleShot(100, this, SLOT(setup_ui()));
}

void OtoEdit::editor_closed() {
  _oto->deleteLater();
  _oto = nullptr;
}
