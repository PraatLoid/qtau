#-------------------------------------------------
#-------------------------------------------------

TEMPLATE = lib
CONFIG  += plugin
TARGET   = $$qtLibraryTarget(otoedit)

QT += widgets

INCLUDEPATH += ../../editor ../ ../../tools




# rename sekai to eprsynth
LIBS += -lsekai -ljack -lsndfile -lqcustomplot

CONFIG      += link_pkgconfig


HEADERS += \
    otoedit.h \
    ../../editor/Utils.h \
    otoeditwindow.h \
    otoini.h \
    ototabcontroller.h

SOURCES += \
    otoedit.cpp \
    ../../editor/Utils.cpp \
    otoeditwindow.cpp \
    otoini.cpp \
    ototabcontroller.cpp

QMAKE_CXXFLAGS += -Wall -std=c++11

#--------------------------------------------
CONFIG(debug, debug|release) {
    COMPILEDIR = $${OUT_PWD}/../../debug
    LIBS += -Wl,--no-undefined
} else {
    COMPILEDIR = $${OUT_PWD}/../../release
    LIBS += -Wl,--no-undefined
}

DESTDIR         = $${COMPILEDIR}/plugins
OBJECTS_DIR     = $${COMPILEDIR}/otoedit/.obj
MOC_DIR         = $${COMPILEDIR}/otoedit/.moc
RCC_DIR         = $${COMPILEDIR}/otoedit/.rcc
UI_DIR          = $${COMPILEDIR}/otoedit/.ui
#--------------------------------------------

#FORMS += \
    

#RESOURCES += \
#    madde.qrc

unix {
    target.path = $${PREFIX}/lib/qtau/plugins
    INSTALLS += target
}

FORMS += \
    otoeditwindow.ui
