#include "otoeditwindow.h"
#include "ui_otoeditwindow.h"
#include "otoini.h"
#include "ototabcontroller.h"
#include "QDebug"
#include <QTabWidget>
#include <QTableView>
#include <QHBoxLayout>
#include <qcustomplot.h>
#include <QLabel>

OtoEditWindow::OtoEditWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OtoEditWindow)
{
    ui->setupUi(this);
    _tabs = new QTabWidget(this);
    _tabs->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _tabs->setContentsMargins(0, 0, 0, 0);
    _tabs->setMaximumSize(9000, 9000);
    _tabs->setTabPosition(QTabWidget::South);
    _tabs->setMovable(false);  // just to be sure
    this->setCentralWidget(_tabs);
}

OtoEditWindow::~OtoEditWindow()
{
    delete ui;
}

void OtoEditWindow::closeEvent(QCloseEvent* event) {
  emit closed();
  QMainWindow::closeEvent(event);
}

void OtoEditWindow::addTab(QString otopath)
{
    QStringList l = otopath.split("/");
    auto tabname = l[l.size()-2];

    OtoIni* otoini = new OtoIni();
    otoini->readFromFile(otopath);
    if(otoini->getCount()==0) return;

    //fill each widget
    QWidget* widget = new QWidget();
    QTableView* table = new QTableView();
    table->setModel(otoini);

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(table);



    widget->setLayout(layout);
    QCustomPlot* plot = new QCustomPlot();
    table->setMaximumWidth(480);

    QWidget* plotcontainer = new QWidget();
    QVBoxLayout* plotlayout = new QVBoxLayout();

    otoToolbarItems* toolbaritems = new otoToolbarItems();

    QToolBar* toolbar = new QToolBar();

    QAction* prev = toolbar->addAction(QIcon(":/images/b_undo.png"),"&p");
    QAction* next = toolbar->addAction(QIcon(":/images/b_redo.png"),"&n");

    toolbaritems->next = next;
    toolbaritems->prev = prev;
    prev->setEnabled(false);

    plotlayout->addWidget(toolbar);
    plotlayout->addWidget(plot);
    plotcontainer->setLayout(plotlayout);

    layout->addWidget(plotcontainer);
    plot->xAxis->setRange(0, 3);
    plot->yAxis->setRange(-1, 1);

    OtoTabController* ctrl = new OtoTabController(widget,plot,otopath,otoini,toolbaritems,table);
    ctrl->buildWaveform(otoini->getEntry(0));

    widget->setLayout(layout);
    _otos.push_back(ctrl);

    connect(prev,&QAction::triggered,ctrl,&OtoTabController::prev);
    connect(next,&QAction::triggered,ctrl,&OtoTabController::next);




    _tabs->addTab(widget,tabname);
}

void OtoEditWindow::on_actionSave_triggered()
{
    foreach( OtoTabController* oto, _otos )
    {
        oto->save();
    }
}
