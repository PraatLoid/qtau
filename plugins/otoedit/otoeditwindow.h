#ifndef OTOEDITWINDOW_H
#define OTOEDITWINDOW_H

#include <QMainWindow>

namespace Ui {
class OtoEditWindow;
}

class OtoTabController;

class OtoEditWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OtoEditWindow(QWidget *parent = nullptr);
    ~OtoEditWindow();
    void addTab(QString otopath);

signals:
 // void valueChanged(QString key, QVariant value);
 // void loadDefault();
 // void loadFile(QString fileName);
 // void saveToFile(QString fileName);
 void closed();
 // void mouseEvent(int status);

private:
    Ui::OtoEditWindow *ui;
    QTabWidget* _tabs;
    QList<OtoTabController*> _otos;
protected:
    void closeEvent(QCloseEvent* event);
private slots:
    void on_actionSave_triggered();
};

#endif // OTOEDITWINDOW_H
