/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#include "otoini.h"
#include "Utils.h"
#define __devloglevel__ 10
#include <QBrush>

//TODO: allow editing -> see dyntablemodel.h

OtoIni::OtoIni()
{

}

void OtoIni::handleKey(QString key, QString value){
    //otoini allows multi key
    otoEnt oto;
    oto.key = key;
    oto.value = value.split(",");
    _entries.append(oto);
}
void OtoIni::handleSection(QString section){
    //ignore atm
}
bool OtoIni::writeOutputToStream(){
    foreach(otoEnt oto,_entries)
    {
        writeKey(oto.key,oto.value.join(","));
    }
}

int OtoIni::rowCount(const QModelIndex &parent) const
{
    (void) parent;
    return _entries.count();
}

int OtoIni::columnCount(const QModelIndex &parent) const
{
    (void) parent;
    return 7;
}

QVariant OtoIni::data(const QModelIndex &index, int role) const
{
    if(role==Qt::DisplayRole || role==Qt::EditRole)
    {
        if(index.column()==0)
            return _entries[index.row()].key;
        else
        {
            int col = index.column()-1;
            auto value = _entries[index.row()].value;
            if(col<value.size())
                return value[col];
        }
    }

    if(role==Qt::BackgroundRole)
    {
        if(index.row()==_sel) return QBrush(Qt::yellow);
    }

   return QVariant();
}

QVariant OtoIni::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole && orientation==Qt::Horizontal)
    {
        switch (section) {
            case 0: return "filename";
            case 1: return "alias";
            case 2: return "offset";
            case 3: return "consonant";
            case 4: return "cutoff";
            case 5: return "preutter";
            case 6: return "overlap";
        default: return "TODO";
        }
    }
    if(role==Qt::DisplayRole && orientation==Qt::Vertical)
    {
        return QVariant(section);
    }
    return QVariant();
}


const otoEnt& OtoIni::getEntry(int index)
{
    qDebug() << "getEntry" << index << _entries.count();
    return _entries[index];
}

uint32_t OtoIni::getCount()
{
    return _entries.size();
}

Qt::ItemFlags OtoIni::flags(const QModelIndex &index) const
{
    if (index.column() == 0)
        {
            return Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
        }
        else
        {
            return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemNeverHasChildren;
        }
}


bool OtoIni::setData(const QModelIndex &index, const QVariant &value, int role)
{
    while(_entries[index.row()].value.size()<index.column())
        _entries[index.row()].value.push_back("");
   _entries[index.row()].value[index.column()-1]=value.toString();
   emit model_changed(index.row(),index.column());
   return true;
}

void OtoIni::setSel(int sel)
{
    _sel = sel;
    emit dataChanged(index(0, 0), index(rowCount(), columnCount()-1));
}

void OtoIni::update(float offset,float consonant,float cutoff,float preutter,float overlap)
{
    while(_entries[_sel].value.size()<6)
        _entries[_sel].value.push_back("");
   _entries[_sel].value[1]=QString("%1").arg(offset*1000.0,0,'f',1);
   _entries[_sel].value[2]=QString("%1").arg(consonant*1000.0,0,'f',1);
   _entries[_sel].value[3]=QString("%1").arg(cutoff*1000.0,0,'f',1);
   _entries[_sel].value[4]=QString("%1").arg(preutter*1000.0,0,'f',1);
   _entries[_sel].value[5]=QString("%1").arg(overlap*1000.0,0,'f',1);
   emit dataChanged(index(0, 0), index(rowCount(), columnCount()-1));
}
