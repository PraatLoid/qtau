/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/

#ifndef MADDEFILE_H
#define MADDEFILE_H

#include <QMap>
#include <QVariant>
#include <QStringList>
#include <QAbstractTableModel>
#include "INIFile.h"

struct otoEnt
{
    QString key;
    QStringList value;
};

class OtoIni : public QAbstractTableModel, public INIFile {
  Q_OBJECT
 private:
  QList<otoEnt> _entries;
  int _sel=0;


 protected:
  virtual void handleKey(QString key, QString value);
  virtual void handleSection(QString section);
  virtual bool writeOutputToStream();

  virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
  virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const;

  virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
  Qt::ItemFlags flags(const QModelIndex &index) const override;


  virtual QString codec() {
      return "UTF8"; //set preffered codec for oto.ini => UTF8 by default, sjis if declared in oto.json
  }



signals:
  void model_changed(int row,int col);


 public:
  OtoIni();
  const otoEnt& getEntry(int index);
  uint32_t getCount();
  void setSel(int sel);
  void update(float offset,float consonant,float cutoff,float preutter,float overlap);
};

#endif  // MADDEFILE_H
