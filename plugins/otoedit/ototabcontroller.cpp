#include "ototabcontroller.h"
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <sndfile.h>
#include <qcustomplot.h>

OtoTabController::OtoTabController(QObject *parent,
                                   QCustomPlot* waveform,
                                   QString otopath,
                                   OtoIni* otoini,
                                   otoToolbarItems* toolbar,
                                   QTableView* table) : QObject(parent),
                                   _waveform(waveform),
                                   _otoini(otoini),
                                   _toolbar(toolbar),
                                   _table(table)
{
   QFileInfo fileinfo(otopath);
   _otopath = fileinfo.absoluteDir().absolutePath();
   _leftblank = new QCPItemRect(_waveform);
   _leftblank->setBrush(QBrush(Qt::gray));
   _consonant = new QCPItemRect(_waveform);
   _consonant->setBrush(QBrush(Qt::yellow));
   _rightblank = new QCPItemRect(_waveform);
   _rightblank->setBrush(QBrush(Qt::gray));
   _greenline = new QCPItemLine(_waveform);
   _greenline->setPen(QPen(Qt::green));
   _redline = new QCPItemLine(_waveform);
   _redline->setPen(QPen(Qt::red));
   connect(waveform,&QCustomPlot::mousePress,this,&OtoTabController::waveform_mousePress);
   connect(waveform,&QCustomPlot::mouseMove,this,&OtoTabController::waveform_mouseMove);
   connect(waveform,&QCustomPlot::mouseRelease,this,&OtoTabController::waveform_mouseRelease);
   connect(otoini,&OtoIni::model_changed,this,&OtoTabController::model_changed);
}

void OtoTabController::buildWaveform(otoEnt oto)
{
    QString filename = _otopath + "/" + oto.key;

    //qDebug() << "show waveform for" << filename;

    //open wav file
    SF_INFO info;
    memset(&info,0,sizeof(info));
    auto sf = sf_open(filename.toUtf8().data(),SFM_READ,&info);
    assert(info.channels==1);

    _length = info.frames * 1.0/info.samplerate;

    if(_waveformdata)
    {
        delete[] _waveformdata;
    }

    //read wav data
    _waveformdata = new float[info.frames];
    _waveformlen = info.frames;
    sf_read_float(sf,_waveformdata,_waveformlen);
    _waveform->xAxis->setRange(0, _length);

    showOto(oto);

    _waveform->clearGraphs();


    int n = info.frames;
    QVector<double> x(n), y(n); // initialize with entries 0..100
    for (int i=0; i<n; ++i)
    {
      x[i] = i*1.0/info.samplerate;
      y[i] = _waveformdata[i];
    }
    // create graph and assign data to it:
    _waveform->addGraph();
    _waveform->graph(0)->setData(x, y);
    _waveform->replot();







}

void OtoTabController::showOto(otoEnt oto)
{
        float offset=0;
        if(oto.value.size()>=2)
        {
            offset=QVariant(oto.value[1]).toFloat()*0.001;
        }
        else
        {
            offset=0.050;
        }
        _params[0]=offset;

        float consonant=0;
        if(oto.value.size()>=3)
        {
            consonant=QVariant(oto.value[2]).toFloat()*0.001;
        }
        else
        {
            consonant=0.100;
        }

        _params[1]=offset+consonant;

        float cutoff=0;
        if(oto.value.size()>=4)
        {
            cutoff=QVariant(oto.value[3]).toFloat()*0.001;
        }
        else
        {
            cutoff=0.50;
        }
        _params[2]=_length-cutoff;


        _leftblank->topLeft->setCoords(0,1);
        _leftblank->bottomRight->setCoords(offset,-1);
        _consonant->topLeft->setCoords(offset,1);
        _consonant->bottomRight->setCoords(offset+consonant,-1);
        _rightblank->topLeft->setCoords(_length-cutoff,1);
        _rightblank->bottomRight->setCoords(_length,-1);

        float preutter=0;
        if(oto.value.size()>=5)
        {
            preutter=QVariant(oto.value[4]).toFloat()*0.001;
        }

        float overlap=0;
        if(oto.value.size()>=6)
        {
            overlap=QVariant(oto.value[5]).toFloat()*0.001;
        }

        float green = offset+preutter;
        _greenline->start->setCoords(green,-0.9);
        _greenline->end->setCoords(green,0.9);
        _params[3]=green;

        float red = offset+overlap;
        _redline->start->setCoords(red,-0.8);
        _redline->end->setCoords(red,0.8);
        _params[4]=red;
}

void OtoTabController::next()
{
    qDebug() << "OtoTabController::next()";
    if(_index>=_otoini->getCount()-1) return;
    _index++;
    buildWaveform(_otoini->getEntry(_index));
    _otoini->setSel(_index);
    updateToolbar();
}
void OtoTabController::prev()
{
    if(_index<0) return;
    _index--;
    _otoini->setSel(_index);
    buildWaveform(_otoini->getEntry(_index));
    updateToolbar();
}

void OtoTabController::updateToolbar()
{
    _toolbar->prev->setEnabled(_index>0);
    _toolbar->next->setEnabled(_index<_otoini->getCount()-1);
}

void OtoTabController::waveform_mouseMove(QMouseEvent *event)
{
    float x = event->x();
    bool found=false;
    if(_mouseEdit==false)
    {
        _selid=-1;
        for(int i=0;i<5;i++)
        {
            if(fabs(x-_waveform->xAxis->coordToPixel(_params[i]))<1)
            {
                found = true;
                _selid=i;
                break;
            }
        }
        if(found)
        {
            setCursor(1);
        }
        else
        {
            if(_mouseEdit==false) setCursor(0);
        }
    }
    else
    {
        if(_selid==0)
        {
            auto p_shift = _waveform->xAxis->pixelToCoord(x-_startx);//cutoff

            float offset = _params[0]+p_shift;
            float consonant = _params[1]+p_shift;

            if(offset>=0 && consonant <= _params[2])
            {
                _leftblank->topLeft->setCoords(0,1);
                _leftblank->bottomRight->setCoords(offset,-1);
                _consonant->topLeft->setCoords(offset,1);
                _consonant->bottomRight->setCoords(consonant,-1);

                _greenline->start->setCoords(_params[3]+p_shift,-0.9);
                _greenline->end->setCoords(_params[3]+p_shift,0.9);
                _redline->start->setCoords(_params[4]+p_shift,-0.8);
                _redline->end->setCoords(_params[4]+p_shift,0.8);
                setCursor(1);
                _shift = p_shift;
            }
            else {
                setCursor(2);
            }

        }
        if(_selid==1)
        {
            auto p = _waveform->xAxis->pixelToCoord(x);//cutoff

            if(p>_params[0] && p<=_params[2])
            {
                _params[1] = p;
                setCursor(1);
            }
            else
            {
                setCursor(2);
            }
            _consonant->topLeft->setCoords(_params[0],1);
            _consonant->bottomRight->setCoords(_params[1],-1);
        }
        if(_selid==2)
        {
            auto p = _waveform->xAxis->pixelToCoord(x);//cutoff

            if(p>_params[1] && p<_length)
            {
                 _params[2] = p;
                setCursor(1);
            }
            else
            {
                setCursor(2);
            }
            _rightblank->topLeft->setCoords(_params[2],1);
            _rightblank->bottomRight->setCoords(_length,-1);

        }
        if(_selid==3)
        {
            auto p = _waveform->xAxis->pixelToCoord(x);
            if(p>0 && p<_length)
            {
                _params[3] = p;
                setCursor(1);
            }
            else
            {
                setCursor(2);
            }
            _greenline->start->setCoords(_params[3],-0.9);
            _greenline->end->setCoords(_params[3],0.9);
        }
        if(_selid==4)
        { 
            auto p = _waveform->xAxis->pixelToCoord(x);
            if(p>0 && p<_length)
            {
                _params[4] = p;
                setCursor(1);
            }
            else
            {
                setCursor(2);
            }
            _redline->start->setCoords(_params[4],-0.8);
            _redline->end->setCoords(_params[4],0.8);
        }
        _waveform->replot();
    }

}
void OtoTabController::waveform_mouseRelease(QMouseEvent *event)
{
    setCursor(0);
    if(_mouseEdit)
    {
        if(_selid==0)
        {
            _params[0] += _shift;
            _params[1] += _shift;
            _params[3] += _shift;
            _params[4] += _shift;
            _shift = 0;
        }
        _mouseEdit=false;

        float offset = _params[0];
        float consonant = _params[1]-offset;
        float cutoff = _length-_params[2];
        float preutter = _params[3]-offset;
        float overlap = _params[4]-offset;

        qDebug() << "offset"<<offset;
        qDebug() << "consonant"<<consonant;
        qDebug() << "cutoff"<<cutoff;
        qDebug() << "preutter"<<preutter;
        qDebug() << "overlap"<<overlap;
        _otoini->update(offset,consonant,cutoff,preutter,overlap);

        //apply changed params
    }
}
void OtoTabController::waveform_mousePress(QMouseEvent *event)
{
    if(_selid>=0) {
        _mouseEdit=true;
        _startx = event->x();
    }
}

void OtoTabController::model_changed(int row,int col)
{
    qDebug() << "model_changed" << row << _index;
    if(row==_index)
    {
        showOto(_otoini->getEntry(row));
        _waveform->replot();
    }
}

void OtoTabController::setCursor(int cursor)
{
    if(_cursor!=cursor)
    {
        if(cursor>0)
        {
            if(cursor==1) QApplication::setOverrideCursor(QCursor(Qt::SizeHorCursor));
            else if(cursor==2) QApplication::setOverrideCursor(QCursor(Qt::ForbiddenCursor));
            else abort();
            _cursor_change_count++;
        }
        else {
            while(_cursor_change_count>0)
            {
                _cursor_change_count--;
                QApplication::restoreOverrideCursor();
            }
        }
        _cursor = cursor;

    }
}

void OtoTabController::save()
{
    qDebug() << "OtoTabController::save()" << _otopath;
    _otoini->writeToFile(_otopath+"/oto.ini");
}
