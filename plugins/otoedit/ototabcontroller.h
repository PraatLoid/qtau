#ifndef OTOTABCONTROLLER_H
#define OTOTABCONTROLLER_H

#include <QWidget>
#include "otoini.h"

class QCustomPlot;
class QTableView;
class QLabel;
class QCPItemRect;
class QCPItemLine;

struct otoToolbarItems
{
    QAction* prev;
    QAction* next;
};

class OtoTabController : public QObject
{
    Q_OBJECT
public:
    explicit OtoTabController(QObject *parent,QCustomPlot* waveform,QString otopath,OtoIni* otoini,otoToolbarItems* toolbar,QTableView* table);
    void buildWaveform(otoEnt oto);
    void showOto(otoEnt oto);
    void save();
public slots:
    void next();
    void prev();
    void waveform_mousePress (QMouseEvent *event);
    void waveform_mouseMove (QMouseEvent *event);
    void waveform_mouseRelease (QMouseEvent *event);
    void model_changed(int row,int col);
private:
    QCustomPlot* _waveform;
    QString _otopath;
    OtoIni* _otoini;
    otoToolbarItems* _toolbar;
    QTableView* _table;
    float* _waveformdata=nullptr;
    uint32_t _waveformlen=0;
    uint32_t _index=0;
    QCPItemRect* _leftblank;
    QCPItemRect* _consonant;
    QCPItemRect* _rightblank;
    QCPItemLine* _redline;
    QCPItemLine* _greenline;
    float _length=0;
    float _params[5];
    int _selid=-1;
    bool _mouseEdit=false;
    float _startx;
    float _shift;
    void updateToolbar();
    void setCursor(int cursor);
    int _cursor=0;
    int _cursor_change_count=0;

};

#endif // OTOTABCONTROLLER_H
