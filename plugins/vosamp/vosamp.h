/*
    This file is part of QTau
    Copyright (C) 2013-2018  Tobias "Tomoko" Platen <tplaten@posteo.de>
    Copyright (C) 2013       digited       <https://github.com/digited>
    Copyright (C) 2010-2013  HAL@ShurabaP  <https://github.com/haruneko>

    QTau is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    SPDX-License-Identifier: GPL-3.0+
*/
#ifndef VOSAMP_H
#define VOSAMP_H

class VoiceSampler;
class QProcess;
class PitchModel;

#include <sinsy/sinsy.h>
#include <sinsy/lyric.h>
#include <sekai/ControlTrack.h>
#include <speex/speex_resampler.h>
#include <QJsonArray>
#include <QList>
#include <QMap>
#include "PluginInterfaces.h"
#include "jack/ringbuffer.h"


class VoSamp : public QObject, public ISynth {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID c_isynth_comname FILE "vosamp.json")
  Q_INTERFACES(ISynth)
  int _jack_samplerate = 0;
  QString _cacheDir;
  QString _voicePath;
  QString _espeakVoice;
  QJsonArray _score;
  IController* _ctrl = nullptr;
  VoiceSampler* _samp = nullptr;
  int _samp_fill = 0;
  int _process_count = 0;
  QList<QProcess*> _scheduledProc;
  QList<QProcess*> _runningProc;
  int _state = 0;
  sinsy::ILyricTimeline* _timeline = nullptr;
  ControlTrack* _ctrack;
  QStringList _espeakFiles;

 public:
  VoSamp() {}

  // plugin loading
  QString name() override;
  QString description() override;
  QString version() override;
  void setup(IController* ctrl) override;

  // synth base
  bool setCacheDir(QString cacheDir) override;
  bool setVoicePath(QString voicePath) override;
  bool synthesize(IScore* score) override;
  bool synthIsRealtime() override;
  int readData(float* data, int size) override;

  // lyricizer
  //QString getTranscription(QString txt) override;
  //bool doPhonemeTransformation(QStringList& lyrics) override;

 private:
  // void lyricize(QString lyric);
  // void lookupPho(QString pho);
  void runESPEAK(QString outfile, QString lyric, int noteNum, QString voice,QString notelength);
  void resumeESPEAK();
  void convertSamplerate(QString infile, QString outfile, int fs);
  void startPlaybackSamplerate();
 signals:
  void signalStatus(int status);
  void logError(QString error);
  void logSuccess(QString success);
  void logDebug(QString debug);

 public slots:
  void on_logError(QString error);
  void on_logSuccess(QString success);
  void on_logDebug(QString debug);
  void processFinished();
};

#define SETSEEK 1
#endif  // LESYNTH_H
