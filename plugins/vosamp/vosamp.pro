#-------------------------------------------------
#-------------------------------------------------

TEMPLATE = lib
CONFIG  += plugin
TARGET   = $$qtLibraryTarget(vosamp)

INCLUDEPATH += ../../editor ../ ../../tools
LIBS += -Wl,--no-undefined

PKGCONFIG += glib-2.0

LIBS += -lsndfile -lsekai -lspeexdsp -lsinsy

CONFIG      += link_pkgconfig


HEADERS += \
    vosamp.h

SOURCES += \
    vosamp.cpp \
    ../../editor/Utils.cpp

QMAKE_CXXFLAGS += -Wall -std=c++11

#--------------------------------------------
CONFIG(debug, debug|release) {
    COMPILEDIR = $${OUT_PWD}/../../debug
} else {
    COMPILEDIR = $${OUT_PWD}/../../release
}

DESTDIR         = $${COMPILEDIR}/plugins
OBJECTS_DIR     = $${COMPILEDIR}/vosamp/.obj
MOC_DIR         = $${COMPILEDIR}/vosamp/.moc
RCC_DIR         = $${COMPILEDIR}/vosamp/.rcc
UI_DIR          = $${COMPILEDIR}/vosamp/.ui
#--------------------------------------------


unix {
    target.path = $${PREFIX}/lib/qtau/plugins
    INSTALLS += target
}
