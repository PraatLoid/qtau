---
title: Project contributors
...

If we forgot to mention you here, let us know and we'll add you. (or if
you don't want to be mentioned, let us know and we'll remove your
entry)

digited
-----------------
Rewrote the current QT gui.

shurabaP
-----------------
Wrote the v.Connect-STAND syntesizer and original audio subsystem.

Tobias "Tomoko" Platen
-----------------
Founder of the QTAU project, and a current maintainer.



