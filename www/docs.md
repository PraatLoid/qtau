---
title: Documentation
...

Since QTAU is backwards compatible with UTAU you should first read the
[UTAU manual](http://utau.wikia.com/wiki/UTAU_User_Manual).

But QTAU has many features that UTAU does not have. 
Those will be documented soon.

There is a shell script in the QTAU source repo that will download 
UTAU voicebanks, convert them to WORLD format and romaji.
Kasane Teto has currently been tested.

QTAU comes with 4 built-in voices that are eSpeak NG based:

+ Mika: Finnish male voice
+ Mikulas: Czech male voice
+ Lukas: Swedish male voice
+ Lenny: English male voice

