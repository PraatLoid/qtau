---
title: Information about sending patches for review
x-toc-enable: true
...

Download QTAU from the Git repository like so:

    $ git clone https://notabug.org/isengaara/qtau.git

You can submit your patches via
[Notabug pull requests](#how-to-submit-your-patches-via-pull-requests).

Editing the website and documentation, wiki-style
-------------------------------------------------

The website and documentation is inside the `www` directory in the
[Git repository](#how-to-download-qtau-from-the-git-repository), in
Pandoc flavoured Markdown. The website is generated into static HTML via Pandoc
with the following scripts in that directory:

- index.sh: generates the news feed (on the News section of the website)
- publish.sh: converts an .md file to an .html file
- Makefile: with calls to index.sh and publish.sh, compiles the entire
  QTAU website

Use any standard text editor (e.g. Geany, Vim, Emacs, Nano, Gedit) to edit the files,
commit the changes and
[send patches](#how-to-submit-your-patches-via-pull-requests).

Optionally, you can install a web server (e.g. lighttpd, nginx) locally and
set the document root to the *www* directory in your local Git repository.
With this configuration, you can then generate your local version of the
website and view it by typing `localhost` in your browser's URL bar.

General guidelines for submitting patches
-----------------------------------------

We require all patches to be submitted under a free license:
<https://www.gnu.org/licenses/license-list.html>.

- GNU General Public License v3 (or any later version) is highly recommended
- For documentation, we require GNU Free Documentation License v1.3 or higher

*Always* declare a license on your work! Not declaring a license means that
the default, restrictive copyright laws apply, which would make your work
non-free.

GNU+Linux is generally recommended as the OS of choice, for QTAU
development.

General code review guidelines
------------------------------

Any member of the public can
[submit a patch](#how-to-submit-your-patches-via-pull-requests).
Members with push access must *never* push directly to the master branch;
issue a Pull Request, and wait for someone else to merge. Never merge your own
work!

Your patch will be reviewed for quality assurance, and merged if accepted.

How to download QTAU from the Git repository
-------------------------------------------------

In your terminal:

    $ git clone https://notabug.org/isengaara/qtau.git

A new directory named `qtau` will have been created, containing
qtau.

How to submit your patches (via pull requests)
----------------------------------------------

Make an account on <https://notabug.org/> and navigate (while logged in) to
<https://notabug.org/isengaara/qtau>. Click *Fork* and in your account,
you will have your own repository of QTAU. Clone your repository, make
whatever changes you like to it and then push to your repository, in your
account on NotABug.

Now, navigate to <https://notabug.org/isengaara/qtau/pulls> and click
*New Pull Request*. 

You can submit your patches there. 

Once you have issued a Pull Request, the QTAU maintainer(s) will be notified
via email. 
