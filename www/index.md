---
title: QTAU
...

[![QTAU Pixelart](alice.png "Alice, the qtau pixel artcharacter"){#alice}](faq.md#who-did-the-ouka-alice-pixelart)

[FAQ](faq.md)                                                               --
[Download](download.md)                                                     --
[Install](install.md)                                                    	--
[Docs](docs.md)                                                             --
[News](news/)                                                               --
[Bugs](https://notabug.org/isengaara/qtau/issues)                      		--
[Send patches](git.md)                                                      --
[Screenshots](screenshots.md)


QTAU is a [free](https://www.gnu.org/philosophy/free-sw.html) (as in
freedom) singing voice synthesizer, similar to VOCALOID and UTAU. 
Since QTAU uses Jack, it integrates well with other DAWS such as Rosegarden
and Ardour. It contains a piano roll editor written in QT and several
synthesizer plugins. 

Instructions for sending patches are on the [git](git.md) page.

QTAU is hackable and easy to use.
------------------

Both VOCALOID and UTAU are proprietary and require the use of a proprietary
operating system. Most modern proprietary operating systems 
[contain](https://www.gnu.org/proprietary/malware-apple.html)
[backdoors](https://www.gnu.org/proprietary/malware-microsoft.html), 
can be slow and have severe bugs and often spy on users. VOCALOID lacks support
for many languages, currelty there are no German voices. 
UTAU allows users to create their own voicebanks, and some resamplers such as
tn_fnds are free. Free user interfaces such as Cadencii have been ported to
GNU/Linux, but most of them are unmaintained and outdated.

QTAU is compatible with UTAU, but it also supports other synthesizers such as 
eSpeak. It reuses existing software such as the MusicXML parser from Sinsy.
Unlike UTAU, QTAU natively supports unicode, so you do not have to switch your
operating system to Japanese Locale to make QTAU work.


