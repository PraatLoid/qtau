---
title: Installing from source
...

Curreltly QTAU can only be installed from source:

    $ sudo apt-get install git build-essential autoconf automake libtool

    $ mkdir ~/qtaubuild

    $ cd ~/qtaubuild
    $ git clone https://notabug.org/isengaara/ecantorix-sinsy-ng
    $ cd ecantorix-sinsy-ng
    $ ./autogen.sh
    $ ./configure
    $ make
    $ sudo make install

    $ cd ~/qtaubuild
    $ sudo apt-get install cmake libsndfile-dev libsamplerate-dev
    $ git clone https://notabug.org/isengaara/sinsy
    $ cd sinsy
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install

    $ cd ~/qtaubuild
    $ git clone https://notabug.org/isengaara/world
    $ cd world
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install

    $ sudo apt-get install libfftw3-dev
    $ git clone https://notabug.org/isengaara/sekai
    $ cd sekai
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install

    $ sudo apt-get install qt5-default libsmf-dev libjack-dev
    $ sudo rm -rf qtau
    $ git clone https://notabug.org/isengaara/qtau
    $ cd qtau
    $ mkdir build
    $ cd build
    $ qmake ..
    $ make
