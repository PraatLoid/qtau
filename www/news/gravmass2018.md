% Sinsy-NG version 0.99 released
% Tobias "Tomoko" Platen
% 25 December 2018

New release of Sinsy-NG

Sinsy-NG version 0.99 NEW!
OLD-based singing voice synthesis system.
It is designed to work as a backend for QTAU.
[Download](https://notabug.org/isengaara/2018-voice/raw/master/sinsy-ng-0.99.tar.gz)

eCantorix-SG Grav-Mass 2018 NEW!
compact speech synthesizer that supports 102 languages and accents.
This is needed for Sinsy-NG
[Download](https://notabug.org/isengaara/2018-voice/src/master/ecantorix-sinsy-ng-gravmass-2018.tar.gz)

New human voice synthesizer implementation
[Download](https://notabug.org/isengaara/2018-voice/raw/master/2018_New_human_voice_synthesizer_implementation.pdf)

