% News announcements from the QTAU project

Updates to QTAU, both technical and organisational, will be written about
here over time. Subscribe to [RSS](/feed.xml) for notifications.

-------------------------------------------------------------------------------

[Sinsy-NG version 0.99 released](gravmass2018.html){.title}
[25 December 2018]{.date}

New release of Sinsy-NG

Sinsy-NG version 0.99 NEW...

[QTAU technical preview by the end of this year](synthesizerv.html){.title}
[18 August 2018]{.date}

Synthesizer V, the Revolutionary Singing Synthesizer was released today...

[QTAU workshop at Anime Marathon 2018](am2018.html){.title}
[01 May 2018]{.date}

There will be a QTAU workshop at the [Anime Marathon 2018](http://marathon.tomodachi.de/)...

