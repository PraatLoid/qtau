% QTAU technical preview by the end of this year
% Tobias "Tomoko" Platen
% 18 August 2018

Synthesizer V, the Revolutionary Singing Synthesizer was released today. 
It is non-free software compiled for Windows and 64 bit Ubuntu 16.04. It runs
on older ThinkPads, but it won't run on my [Talos II](https://raptorcs.com/).

By contrast QTAU and it's dependencies are released under various GPLv3
compatible licenses. QTAU is an Evolutionary Singing Synthesizer that will 
remain free as in freedom. QTAU includes an engine called eCantorix2 that
will even run on weak computers such as the BeagleBone Black and the 
[EOMA68-A20](https://www.crowdsupply.com/eoma68/micro-desktop).

I plan to include a technical preview in GuixSD by the end of this year.
The current version already compiles with Guix:
<https://notabug.org/isengaara/ongakunix-guix>

Unlike Synthesizer V, QTAU includes support for many European languages such as
Swedish, Czech and German. Since it mainly uses eSpeak-NG as a synthesizer
it also supports English, Mandarin Chinese and in recent versions Japanese.
For corpus based synthesis v.Connect-STAND can be used with UTAU compatible
voicebanks, including 
[Namine Ritsu Connect](http://hal-the-cat.music.coocan.jp/ritsu_e.html).
v.Connect-STAND produces a much more natural sound than most formant based
singing synthesizers. 

QTAU can also run without a GUI and act as a replacement for
[Milan Zamazal's Singing Computer](https://freebsoft.org/singing-computer)


